export const tickersListUpdate = state => {
	for (const deletedTicker of state.deletedTickers) {
		state.tickers = state.tickers.filter(
			ticker => ticker.ticker !== deletedTicker.ticker
		);
	}
};
