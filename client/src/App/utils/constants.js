export const tickersNames = {
	AAPL: 'Apple',
	GOOGL: 'Alphabet',
	MSFT: 'Microsoft',
	AMZN: 'Amazon',
	BA: 'Boeing Co',
	TSLA: 'Tesla',
	EPAM: 'EPAM',
	BABA: 'Alibaba',
	META: 'Meta',
	NFLX: 'Netflix',
	RIVN: 'Rivian',
	NVDA: 'NVIDIA',
};

export const THOUSAND_MS = 1000;
export const TIME_INTERVAL_5s = 5000;
export const TIME_INTERVAL_15s = 15000;
export const TIME_INTERVAL_30s = 30000;
export const TIME_INTERVAL_60s = 60000;
export const LOGO_TEXT = 'My Finance App';
