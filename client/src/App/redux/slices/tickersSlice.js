import { createSlice } from '@reduxjs/toolkit';
import { TIME_INTERVAL_5s } from '../../utils/constants';
import { tickersListUpdate } from '../../utils/helper';

const tickersSlice = createSlice({
	name: 'tickers',
	initialState: {
		tickers: [],
		deletedTickers: [],
		timeForUpdate: TIME_INTERVAL_5s,
	},
	reducers: {
		setTickers: (state, { payload }) => {
			state.tickers = payload;
			if (state.deletedTickers.length > 0) {
				tickersListUpdate(state);
			}
		},
		deleteTicker(state, { payload }) {
			state.deletedTickers.push(payload);
			tickersListUpdate(state);
		},
		addTicker(state, { payload }) {
			state.tickers = state.tickers.concat(payload);
			state.deletedTickers = state.deletedTickers.filter(
				ticker => ticker.ticker !== payload.ticker
			);
		},
		changeUpdatingTime(state, { payload }) {
			state.timeForUpdate = payload;
		},
	},
});

export const { setTickers, deleteTicker, addTicker, changeUpdatingTime } =
	tickersSlice.actions;
export default tickersSlice.reducer;
