import React from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { getTickersList } from '../../redux/selectors';
import { addTicker } from '../../redux/slices/tickersSlice';

import { tickersNames } from '../../utils/constants';

import './Sidebar.css';

const Sidebar = () => {
	const dispatch = useDispatch();
	const { deletedTickers } = useSelector(getTickersList);

	const addToWatchingGroup = ticker => {
		dispatch(addTicker(ticker));
	};

	return (
		<div className='sidebar'>
			<h1 className='sidebar-title'>Removed tickers:</h1>
			{deletedTickers.map(ticker => (
				<div className='ticker-container' key={ticker.ticker}>
					<div className='tickerName'>{tickersNames[ticker.ticker]}</div>
					<button
						className='ticker-add-btn'
						onClick={() => {
							addToWatchingGroup(ticker);
						}}
					>
						&#43;
					</button>
				</div>
			))}
		</div>
	);
};

export default Sidebar;
