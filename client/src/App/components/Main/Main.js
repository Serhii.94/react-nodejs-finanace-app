import React, { useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { getTickersList, getUpdateTime } from '../../redux/selectors';
import { setTickers } from '../../redux/slices/tickersSlice';

import TickerItem from '../TickerItem/TickerItem';

import { socket } from '../../socket';

import './Main.css';

const Main = () => {
	const dispatch = useDispatch();
	const { tickers } = useSelector(getTickersList);

	const timeForUpdate = useSelector(getUpdateTime);

	useEffect(() => {
		socket.emit('start', timeForUpdate);
		socket.on('ticker', res => {
			dispatch(setTickers(res));
		});
	}, [dispatch, timeForUpdate]);

	return (
		<main className='main'>
			{tickers.length === 0 ? (
				<div className='no-tickers'>
					No tickers found !<br /> You can add it from left sidebar
				</div>
			) : (
				tickers.map(ticker => (
					<TickerItem ticker={ticker} key={ticker.ticker} />
				))
			)}
		</main>
	);
};
export default Main;
