import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteTicker } from '../../redux/slices/tickersSlice';

import { tickersNames } from '../../utils/constants';

import './TickerItem.css';

const TickerItem = ({ ticker }) => {
	const dispatch = useDispatch();

	const removeTicker = tickerName => {
		dispatch(deleteTicker(tickerName));
	};

	const { price, change, change_percent, last_trade_time } = ticker;
	const priceChanged = (price - change).toFixed(2);

	const dateFormatter = date => {
		const unixTimeZero = Date.parse(date);
		const dateUTC = new Date(unixTimeZero);
		const dateStringUTC = dateUTC.toUTCString();
		return dateStringUTC;
	};

	const formatedDate = dateFormatter(last_trade_time);

	return (
		<div className='ticker'>
			<button
				className='ticker-btn-delete'
				title='remove'
				onClick={() => {
					removeTicker(ticker);
				}}
			>
				&#10006;
			</button>
			<header className='ticker-header'>
				<h1 className='ticker-title'>{tickersNames[ticker.ticker]}</h1>
				<span className='ticker-precent'>{change_percent} %</span>
			</header>
			<main className='ticker-main'>
				<span className='ticker-price'>price: {price} $</span>
				<span
					className={`ticker-change ${
						priceChanged > 0 ? 'border-green ' : 'border-red'
					}`}
				>
					{priceChanged} $
					<strong
						className={`ticker-change_number
							${priceChanged > 0 ? 'arrow-green' : 'arrow-red'}
						`}
					>
						{priceChanged > 0 ? '⇧' : '⇩'}
					</strong>
				</span>
			</main>
			<footer className='ticker-footer'>
				<span>{formatedDate}</span>
			</footer>
		</div>
	);
};

export default TickerItem;
