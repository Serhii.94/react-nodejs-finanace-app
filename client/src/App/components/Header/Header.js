import React from 'react';
import { useDispatch } from 'react-redux';
import { changeUpdatingTime } from '../../redux/slices/tickersSlice';

import { socket } from '../../socket';
import {
	LOGO_TEXT,
	THOUSAND_MS,
	TIME_INTERVAL_15s,
	TIME_INTERVAL_30s,
	TIME_INTERVAL_5s,
	TIME_INTERVAL_60s,
} from '../../utils/constants';

import './Header.css';

const Header = () => {
	const reloader = () => window.location.reload();
	const dispatch = useDispatch();

	const changeTimer = time => {
		dispatch(changeUpdatingTime(time));
		socket.disconnect();
		socket.connect(time);
	};

	const buttonsList = [
		TIME_INTERVAL_5s,
		TIME_INTERVAL_15s,
		TIME_INTERVAL_30s,
		TIME_INTERVAL_60s,
	];

	return (
		<header className='header'>
			<h1 className='header__logo' onClick={reloader}>
				{LOGO_TEXT}
			</h1>
			<div className='btn-block'>
				{buttonsList.map(interval => (
					<button
						key={interval}
						className='btn-block__btn'
						onClick={() => {
							changeTimer(interval);
						}}
					>
						{interval / THOUSAND_MS} s.
					</button>
				))}
			</div>
		</header>
	);
};

export default Header;
