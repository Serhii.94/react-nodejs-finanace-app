export const getTickersList = state => state.tickers;
export const getUpdateTime = state => state.tickers.timeForUpdate;
