import React from 'react';

import Header from './components/Header/Header';
import Main from './components/Main/Main';
import Footer from './components/Footer/Footer';
import Sidebar from './components/Sidebar/Sidebar';

import './App.css';

import { useSelector } from 'react-redux';
import { getTickersList } from './redux/selectors';

function App() {
	const { deletedTickers } = useSelector(getTickersList);
	const widthFR = deletedTickers.length !== 0 ? '1fr 4fr' : '0fr 4fr';

	const myStyles = {
		minHeight: '100%',
		display: 'grid',
		grid: "'header header' 'sidebar main' 'footer footer'",
		gridTemplateColumns: widthFR,
	};

	return (
		<div className='App' style={myStyles}>
			<Header />
			<Main />
			{deletedTickers.length !== 0 ? <Sidebar /> : null}
			<Footer />
		</div>
	);
}

export default App;
