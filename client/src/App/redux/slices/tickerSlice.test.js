import { setTickers } from './tickersSlice';

const testState = {
	ticker: 'ticker Test',
	exchange: 'exchange Test',
	price: 1,
	change: 1,
	change_percent: 1,
	dividend: 1,
	yield: 1,
	last_trade_time: 'trade_time Test',
};

describe('Tickers test', () => {
	it('Action setTickers shoud works with testState', () => {
		const testSetTickers = setTickers([testState]);
		expect(testSetTickers).toEqual({
			type: 'tickers/setTickers',
			payload: [testState],
		});
	});

	it('Action setTickers shoud works with empty State', () => {
		const testSetTickers = setTickers([]);
		expect(testSetTickers).toEqual({ type: 'tickers/setTickers', payload: [] });
	});
});
